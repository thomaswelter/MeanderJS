# Meander js
Is a streaming library where functions can contain state, and where you can easily write your own functions.

## Demo
you can find an interactive explanation of the library [here][1]

[1]: https://thomaswelter.gitlab.io/MeanderJS

## Api
- `Meander('.some-element')`
  listen to click events on the element

- `Meander('input.element')`
  listen to input events (also works on textarea)
  
- `Meander(document)`
  listen for DOMContentLoaded event

- `Meander(window)`
  listen for load event
 
- `Meander(Element)`
  listen for click event or input event on input/textarea

- `Meander('http://example.com')`
  fetch example.com and return resolved response object
  
- `Meander(next => setTimeout(next, 1000))`
  run function which decides when the next operator downstream gets called

- `Meander(23)`
  send value 23 downstream
  
- `Meander([1, 'two', [3]])`
  downstream function will receive `1` >> `'two'` >> `[3]` (in that order)
  
- `Meander(Promise.resolve(4))`
  resolves promise and sends it value downstream

- `.value`
  the latest value this node received

- `.fn(emit => input => emit(input))`
  define custom operator. The function is first called with an emit callback on initialization, the result of that should be a function that will be called with subsequent values.

- `.fnAll(emit => input => emit(input)`
  same as fn but will also be called when input is an instanceof Error

- `.catch(err => 0)`
  function is only called when it receives an error

- `.retry(5)`
  if value is an error start again from the root, do this 5 times

- `.log(2)`
  log to console and pass on

- `.map(x => x + 1)`
  maps over value: 4 becomes 5

- `.reduce((acc, val) => acc + val, 0)`
  fold over value. example acts like a counter

- `.filter(x => x > 3)`
  filter value. next is called when x is higher then 3

- `.delay(500)`
  call next after 500 ms

- `.combine(500)`
  combine all values received withing 500ms

- `.throttle(500)`
  buffer value if throttle has been called in the last 500ms

- `.distinct()`
  only call next if value is different then the previous value

- `.first(4)`
  only send the first 4 values

- `.skip(3)`
  skip the first 3 values

- `.timestamp()`
  sends `Date.now()` as a value

- `.count()`
  start at 0 and add one on ech trigger

- `.average()`
  sends a running average

- `.sum()`
  returns the sum of all input

- `.min()`
  returns the smallest seen value

- `.max()`
  returns the largest seen value

- `.test(x => x > 18)`
  test value with function and returns boolean value

- `.hold('unique')`
  buffer all values until 'unique' is encountered, then release and empty buffer. 'unique' is not included

- `.debounce(500)`
  discard value if debounce has been called in the last 500ms

- `.dom('.selector')`
  set innerHTML of `.selector` to received value or set value attribute if `.selector` is an input or textarea

- `.dom((value) => ({h1: value + ' is the value'}))`
  set all innerHTML of h1 elements on the page to value

- `.dom(selector, fn)`
  ```javascript
  .dom('#messages', (item, index, wrapper) => ({
      h3: item.title,
      '.author': 'by ' + item.author,
      button: el => el.addEventListener('click', () => console.log('see more from', item))
  }))
  ```
  should receive an array. The nodes of `#messages` are duplicated for every item in the array.