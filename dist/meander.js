var Meander = (function () {
'use strict';

function Meander$1() {
	var this$1 = this;
	var i = arguments.length, argsArray = Array(i);
	while ( i-- ) argsArray[i] = arguments[i];

	if(!(this instanceof Meander$1))
		{ return new (Function.prototype.bind.apply( Meander$1, [ null ].concat( argsArray) )) } // no new keyword needed

	var args = Array.prototype.slice.call(arguments);
	var root = this;

	/*
		every new method returns a new object with itself as the prototype.
		A reference to these objects is kept in contextReference.
		it's value might look like this (the names are actualy object references):
		
		let chain = Meander().map()
		chain.reduce()
		chain.combine().log()

		{
			root: [map],
			map: [reduce, combine],
			reduce: [],
			combine: [log],
			log: []
		}
	*/
	var contextReference = new Map([[this, []]]);

	if(Meander$1.debugger && Meander$1.debugger.active)
		{ Meander$1.debugger.addInit(this, 'Meander', args); }

	/*
		context: [input => emit( input ), 'name', ['input arguments', 500]]

		the methodReference of log looks like this:
		log: [input => (console.log(input), emit(input)), 'log', []]
	*/
	var methodReference = new Map;

	// stack that holds next method call
	var nextFnStack = [];

	this._convertMethod = function (name, context, args, fn) {
		var signature = Object.create(context);

		// prevent value from looking in prototype
		signature.value = undefined;

		// significant performance tweak on long chains, prevents walking the prototype chain
		signature._convertMethod = this$1._convertMethod;

		// reference to prev child
		signature._prev = this$1;

		contextReference.get(context).push(signature);
		contextReference.set(signature, []);

		if(Meander$1.debugger && Meander$1.debugger.active)
			{ Meander$1.debugger.addInit(signature, name, args); }

		// emit can fire multiple values
		var emit = function () {
			var args = [], len = arguments.length;
			while ( len-- ) args[ len ] = arguments[ len ];

			return args.forEach(function (arg) { return next(signature)(arg); });
		};

		// fn is now: input => emit( )
		var setupFn = fn.apply(void 0, args)( emit, signature );

		// args is kept only for debugging
		methodReference.set(signature, setupFn);

		return signature
	};

	// push is not chainable and does not add to the contextReference, so it's defined seperately
	this.push = function () {
		var args = [], len = arguments.length;
		while ( len-- ) args[ len ] = arguments[ len ];

		return args.forEach( function (arg) { return next(root)(arg); } );
	};

	// access root trough this property
	this._root = root;

	// avoid recursion (stack overflow) by using a function stack
	var next = function (signature) { return function (input) {
		nextFnStack.push(next_wrapped(signature)(input));

		if(nextFnStack.length > 1)
			{ return }

		while(nextFnStack.length) {
			nextFnStack[0]();
			nextFnStack.shift();
		}
	}; };

	// call the next method's function, input is the value of the previous method
	var next_wrapped = function (signature) { return function (input) { return function () {
		// assign last value
		signature.value = input;

		if(Meander$1.debugger && Meander$1.debugger.active)
			{ Meander$1.debugger.addCall(signature, input); }

		// if this is the last method and the value is an error it is uncaught
		if(!contextReference.get(signature).length && input instanceof Error)
			{ console.error('Uncaught Error: ' + input.message + '\n\tin Meander'); }

		var execNext = function () {
			// executes the actual function
			// does not need to return a values
			// the next value is obtained by calling the emit function (async)
			var activate = function (signature) { return function (input) {
				var fn = methodReference.get(signature);

				try { fn(input); }
				catch(e) {
					next(signature)(Error(e
						+ '\n\tin .'
						+ name
						+ '( '
						+ args.join('')
						// + formatValueToString(args, false, true, 50)
						+ ')'
					));
				}
			}; };

			// resolve Promises and Meanders before proceeding
			contextReference.get(signature).forEach(function (child) {
				if(input instanceof Promise)
					{ input.then( activate(child), activate(child) ); }

				else if(input instanceof Meander$1)
					{ input._debuggerIgnore( activate(child) ); }

				else
					{ activate(child)(input); }
			});
		};

		if(Meander$1.debugger && Meander$1.debugger.pause)
			{ Meander$1.debugger(execNext); }

		else
			{ execNext(); }
	}; }; };

	// then create event contextReference from arguments
	args.forEach(function (arg) {
		var listen = next(this$1);

		var isUrl = function (str) { return /^http:\/\//.test(str); };

		if(typeof window !== 'undefined' && typeof module == 'undefined') {
			var bindEvent = function (el) {
				var events = {
					'INPUT': 'input',
					'TEXTAREA': 'input'
				};

				return events[el.nodeName] || 'click'
			};
				
			if( typeof arg == 'string' && !isUrl(arg)) {
				var el = document.querySelector(arg);
				el.addEventListener(bindEvent(el), listen);
			}

			if( arg === document )
				{ arg.addEventListener('DOMContentLoaded', listen); }

			if( arg === window )
				{ arg.addEventListener('load', listen); }

			if( arg instanceof Element)
				{ arg.addEventListener(bindEvent(arg), listen); }
		}

		if( typeof arg == 'string' && isUrl(arg))
			{ fetch(new Request(arg)).then( listen ); }

		if( typeof arg == 'function' )
			{ arg( listen ); }

		if( typeof arg == 'number' )
			{ setTimeout(function () { return listen( arg ); }, 0); } // wait one cycle to give script time to attach follow up function

		if( Array.isArray(arg) )
			{ setTimeout(function () { return arg.forEach( listen ); }, 0); }

		if( arg instanceof Promise )
			{ arg.then( listen, listen ); }

		if( arg instanceof Meander$1)
			{ arg._debuggerIgnore( listen ); }
	});

	return this
}

var extendWithError = function (object) {
	// convert the Meander.prototype functions into chainable form
	var loop = function ( key ) {
		Meander$1.prototype[key] = function() {
			return this._convertMethod(key, this, Array.prototype.slice.call(arguments), object[key])
		};
	};

	for(var key in object) loop( key );
};

var extend = function (object) {
	// convert the Meander.prototype functions into chainable form
	var filterError = function (fn) { return function () {
		var args = [], len = arguments.length;
		while ( len-- ) args[ len ] = arguments[ len ];

		return function (emit) {
		var setupFn = fn.apply(void 0, args)(emit);

		return function (input) {
			if(input instanceof Error)
				{ return emit( input ) }

			setupFn( input );
		}
	};
 }		};

	var loop = function ( key ) {
		Meander$1.prototype[key] = function() {
			return this._convertMethod(key, this, Array.prototype.slice.call(arguments), filterError(object[key]))
		};
	};

	for(var key in object) loop( key );
};

var fn = function (fn) { return function (emit) { return fn(emit); }; };

var map = function (fn) { return function (emit) { return function (input) { return emit( fn(input) ); }; }; };

var reduce = function (fn, initial) { return function (emit) {
	var acc = initial;

	return function (input) { return emit( acc = fn(acc, input) ); }
}; };

var filter = function (fn) { return function (emit) { return function (input) { return fn(input) && emit(input); }; }; };

var delay = function (ms) { return function (emit) { return function (input) { return setTimeout(function () { return emit( input ); }, ms); }; }; };

var combine = function (ms) { return function (emit) {
	var buffer = [];

	return function (input) {
		if(!buffer.length)
			{ setTimeout(function () { return (emit(buffer), buffer = []); }, ms); }

		buffer.push(input);
	}
}; };

var throttle = function (ms) { return function (emit) {
	var buffer = [];
	var running = false;

	var next = function () { return setTimeout(function () {
		if(buffer.length) {
			emit( buffer.shift() );
			return next()
		}

		running = false;
	}, ms); };

	return function (input) {
		buffer.push( input );

		if(running)
			{ return }

		running = true;
		emit( buffer.shift() );
		next();
	}
}; };

var distinct = function () { return function (emit) {
	var prev = undefined;

	return function (input) {
		if(prev === input)
			{ return }

		prev = input;
		emit( input );
	}
}; };

var first = function (n) {
	if ( n === void 0 ) n = 1;

	return function (emit) {
	var count = 0;

	return function (input) {
		if(count >= n)
			{ return }

		count += 1;
		emit( input );
	}
};
};

var skip = function (n) { return function (emit) {
	var skipped = 0;

	return function (input) {
		if(skipped >= n)
			{ return emit( input ) }

		skipped++;
	}
}; };

var timestamp = function () { return function (emit) { return function (input) { return emit( Date.now() ); }; }; };

var count = function () { return function (emit) {
	var counter = 0;

	return function (input) {
		counter++;

		emit( counter );
	}
}; };

var average = function () { return function (emit) {
	var count = 0;
	var average = 0;

	return function (input) {
		if(typeof input !== 'number')
			{ return emit(Error('average: input was not a number')) }

		count++;
		average = ((count -1 ) * average + input) / count;
		emit( average );
	}
}; };

var sum = function () { return function (emit) {
	var total = 0;

	return function (input) {
		if(typeof input !== 'number')
			{ return emit(Error('sum: input was not a number')) }

		total += input;
		emit( total );
	}
}; };

var min = function () { return function (emit) {
	var min = Infinity;

	return function (input) {
		if(typeof input !== 'number')
			{ return emit(Error('min: input was not a number')) }

		min = Math.min(min, input);
		emit( min );
	}
}; };

var max = function () { return function (emit) {
	var max = -Infinity;

	return function (input) {
		if(typeof input !== 'number')
			{ return emit(Error('max: input was not a number')) }

		max = Math.max(max, input);
		emit( max );
	}
}; };

var test = function (fn) { return function (emit) { return function (input) { return emit( !!fn(input) ); }; }; };

var hold = function (token) { return function (emit) {
	var buffer = [];

	return function (input) {
		if(input === token) {
			emit.apply( void 0, buffer );
			buffer = [];
			return
		}

		buffer.push( input );
	}
}; };

var debounce = function (ms) { return function (emit) {
	var running = false;

	return function (input) {
		if(running)
			{ return }

		emit( input );
		running = true;
		setTimeout(function () { return running = false; }, ms);
	}
}; };

/*
	dom value to HTML, example ussage:
	
	input = [
		{body: 'example', likes: 57, time: 234234},
		{body: 'example 2', likes: 42, time: 105352}
	]

	<ul id="comments">
		<p>comment body</p>
		<button>Remove</button>
		<span>0 Likes</span>
		<span>some time ago</span>
	</ul>

	const listen = (event, fn) => element => element.addEventListener(event, fn)

	.dom('#comments', (item, i, wrapper) => ({
		p 		: item.body
		button	: first && listen('click', data.remove(i)),
		span 	: [item.likes + ' Likes', new Date(item.time)]
	}))
*/
var dom = function () {
	var args = [], len = arguments.length;
	while ( len-- ) args[ len ] = arguments[ len ];

	return function (emit) {
/*
	if(typeof fn !== 'function')
		throw Error('fn should be a function\n\t.dom( prefix, fn )')*/

	var isFn = function (val) { return typeof val == 'function'; };
	var isValue = function (val) { return ['string', 'number'].indexOf(typeof val) > -1; };
	var asArray = function (val) { return Array.isArray(val)? val : [val]; };
	var takeFirst = function (arr, fn) { return arr[0] && fn(arr[0]); };
	var runIf = function (fn1, question, fn2) { return question? fn1.map(fn2) : fn1; };
	var setInnerHTML = function (element) { return function (value) { return element.innerHTML = value; }; };
	var forEachTag = function (fn, element) { return function (ref) {
		var tagName = ref[0];
		var value = ref[1];

		return Array.prototype.slice.call(element.querySelectorAll(tagName)).map(fn(value));
 }		};
	var runWithElement = function (first) { return function (value) { return function (element) {
		var round = runIf(
			asArray(value).map(function (value) { return isFn(value)? value(element) : value; }),
			first,
			function (value) { return isFn(value)? value() : value; }
		);

		takeFirst( round.filter(isValue), setInnerHTML(element) );
	}; }; };

	// Function only mode
	if(isFn(args[0])) {
		var first = true;
		var fn = args[0];

		return function (input) {
			Object.entries(fn(input))
				.forEach( forEachTag(runWithElement(first), document) );

			first = false;
			emit(input);
		}
	}

	// use prefix as template mode
	else if(isFn(args[1])) {
		var prefix = args[0];
		var fn$1 = args[1];
		var domRef = [];

		var element = prefix instanceof Element
			? prefix
			: document.querySelector(prefix);
		var template = element.innerHTML;
		element.innerHTML = '';

		return function (input) {
			asArray(input).forEach(function (item, i) {
				// item = {title: 'first title', active: true}

				// search for item in domReference
				var itemAtIndex = domRef.map(function (x) { return x[0]; }).indexOf(item);

				var first = false;
				var ref = itemAtIndex == -1
					? [item, false]
					: domRef.splice(itemAtIndex, 1)[0];
				var value = ref[0];
				var wrapper = ref[1];

				// create if non existent
				if(!wrapper) {
					first = true;
					wrapper = document.createElement('span');
					wrapper.innerHTML = template;
					element.appendChild(wrapper);
				}

				// place at the right index
				wrapper.parentNode.insertBefore(wrapper, wrapper.parentNode.children[i]);

				/*
					selectorObject = {
						h1: 'first title',
						'.item': element => element.style.backgroundColor = 'green'
					}
				*/
				var selectorObject = fn$1(item, i, wrapper);

				if(typeof selectorObject !== 'object')
					{ throw Error('fn should return an Object\n\t.dom( prefix, fn )') }

				Object.entries(selectorObject)
					.forEach( forEachTag(runWithElement(first), wrapper) );
			});

			// remove unused nodes
			domRef.forEach(function (item) { return item[1].remove(); });

			// reference for next time, include wrappers
			domRef = asArray(input).map(function (x, i) { return [x, element.children[i]]; });

			emit( input );
		}
	}

	// simple mode, set only one tagName
	else {
		var tagName = args[0];
		return function (input) {
			isValue(input)
				&& forEachTag(function (value) { return function (element) {
					['INPUT', 'TEXTAREA'].indexOf(element.nodeName) == -1
						? element.innerHTML = value
						: element.value = value;
				}; }, document)([tagName, input])
				&& emit(input);
		}
	}
};
};

var fnAll = function (fn) { return function (emit) { return fn(emit); }; };

var log = function (fn) { return function (emit) { return function (input) { return (console.log(input), fn && fn(input), emit(input)); }; }; };

// variables cannot be named 'catch'
var $catch = function (fn) { return function (emit) { return function (input) {
	if(input instanceof Error)
		{ return emit( fn(input) ) }

	emit( input );
}; }; };

var retry = function (maxTries) {
	if ( maxTries === void 0 ) maxTries = Infinity;

	return function (emit, context) {
	var tries = 0;

	return function (input) {
		if(input instanceof Error && tries < maxTries) {
			setTimeout(function () { return context.push( context._root.value ); }, 0);
			tries++;
			return
		}

		emit( input );
	}
};
};

var _debuggerIgnore = function (fn) { return function (emit) { return function (input) { return emit( fn( input ) ); }; }; };

Meander$1.extend = extend;
Meander$1.extendWithError = extendWithError;

Meander$1.extend({
	fn: fn,
	map: map,
	reduce: reduce,
	filter: filter,
	delay: delay,
	combine: combine,
	throttle: throttle,
	distinct: distinct,
	first: first,
	skip: skip,
	timestamp: timestamp,
	count: count,
	average: average,
	sum: sum,
	min: min,
	max: max,
	test: test,
	hold: hold,
	debounce: debounce,
	dom: dom
});

Meander$1.extendWithError({
	fnAll: fnAll,
	log: log,
	catch: $catch, // catch is a reserved keyword
	retry: retry,
	_debuggerIgnore: _debuggerIgnore
});

if(typeof window === 'undefined')
	{ module.exports = Meander$1; }

return Meander$1;

}());
//# sourceMappingURL=meander.js.map
