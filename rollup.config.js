import buble from 'rollup-plugin-buble'

export default {
	input: 'src/index.js',
	output: {
		file: 'dist/meander.js',
		format: 'iife',
		name: 'Meander',
		sourcemap: true
	},
	plugins: [
		buble()
	]
};