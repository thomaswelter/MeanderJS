import test from 'ava'
import Meander from '../dist/meander.js'
// import Meander from '../dist/meander.min.js'

// Basic tests
test('meander instance', t => {
	t.true(Meander() instanceof Meander)
})

test.cb('map receives number', t => {
	Meander(1).map(x => {
		t.is(x, 1)
		t.end()
	})
})

test.cb('.value gives the latest value', t => {
	const m = Meander([1,2,3,4])

	setTimeout(() => {
		t.is(m.value, 4)
		t.end()
	}, 100)
})

// fn method
test.cb('fn setup is remembered', t => {
	const m = Meander([1,1,1])
	.fn(emit => {
		let count = 2
		return input => {
			count++
			emit( count )
		}
	})

	setTimeout(() => {
		t.is(m.value, 5)
		t.end()
	}, 100)
})

// map method
test.cb('map fn is run', t => {
	Meander(4)
	.map(x => x + 2)
	.map(x => {
		t.is(x, 6)
		t.end()
	})
})

test.cb('map should not exec on error value', t => {
	Meander([Error('fake')])
	.map(x => x + 2)
	.map(t.fail)
	.catch(x => t.end())
})

test.cb('wait for promise', t => {
	Meander(3)
	.map(x => new Promise(s => setTimeout(s, 10, x * 2)))
	.map(x => {
		t.is(x, 6)
		t.end()
	})
})

test.cb('exec after catch', t => {
	Meander([Error('fake')])
	.map(x => x + 2)
	.map(t.fail)
	.catch(x => 5)
	.map(x => t.end())
})

// reduce method
test.cb('reduce', t => {
	const m = Meander([4,5,6])
	.reduce((acc, n) => acc + n, 0)

	setTimeout(() => {
		t.is(m.value, 15)
		t.end()
	}, 100)
})

// filter method
test.cb('filter', t => {
	const m = Meander([1,2])
	.filter(x => x % 2 == 0)

	setTimeout(() => {
		t.is(m.value, 2)
		t.end()
	}, 100)
})

// delay
test.cb('delay', t => {
	t.plan(2)

	const m = Meander([2])
	.delay(300)

	setTimeout(() => t.is(m.value, undefined), 200)
	setTimeout(() => {
		t.is(m.value, 2)
		t.end()
	}, 700)
})

// combine method
test.cb('combine', t => {
	Meander([1,2,3])
	.combine(100)
	.map(x => {
		t.deepEqual(x, [1,2,3])
		t.end()
	})
})

// throttle
test.cb('throttle', t => {
	t.plan(3)

	const m = Meander([1,2,3])
	.throttle(300)

	setTimeout(() => t.is(m.value, 1), 50)
	setTimeout(() => t.is(m.value, 2), 500)
	setTimeout(() => {
		t.is(m.value, 3)
		t.end()
	}, 700)
})

// distinct
test.cb('distinct', t => {
	const m = Meander([1,2,2,3])
	.distinct()
	.count()

	setTimeout(() => {
		t.is(m.value, 3)
		t.end()
	}, 100)
})

// first method
test.cb('first', t => {
	const m = Meander([1,2,3,4])
	.first(2)

	setTimeout(() => {
		t.is(m.value, 2)
		t.end()
	}, 100)
})

//skip
test.cb('skip', t => {
	const m = Meander([1,2,3,4])
	.skip(2)
	.count()

	setTimeout(() => {
		t.is(m.value, 2)
		t.end()
	}, 100)
})

// timestamp
test.cb('timestamp', t => {
	Meander([1])
	.timestamp()
	.map(x => {
		t.is((x + '').length, 13)
		t.end()
	})
})

// average
test.cb('average', t => {
	const m = Meander([0,10,10,0])
	.average()

	setTimeout(() => {
		t.is(m.value, 5)
		t.end()
	}, 100)
})

// sum
test.cb('sum', t => {
	const m = Meander([1,2,3])
	.sum()

	setTimeout(() => {
		t.is(m.value, 6)
		t.end()
	}, 100)
})

// min
test.cb('min', t => {
	const m = Meander([9,6,3,6,2,7])
	.min()

	setTimeout(() => {
		t.is(m.value, 2)
		t.end()
	}, 100)
})

// max
test.cb('max', t => {
	const m = Meander([9,6,3,6,2,7])
	.max()

	setTimeout(() => {
		t.is(m.value, 9)
		t.end()
	}, 100)
})

// test
test.cb('test', t => {
	Meander([5])
	.test(x => x == 5)
	.map(x => {
		t.true(x)
		t.end()
	})
})

// hold
test.cb('hold', t => {
	const m = Meander([1,2,'release',3,4])
	.hold('release')
	.combine(10)

	setTimeout(() => {
		t.deepEqual(m.value, [1,2])
		t.end()
	}, 100)
})

// debounce
test.cb('debounce method', t => {
	const m = Meander([1,2,3])
	.debounce(30)

	setTimeout(() => {
		t.is(m.value, 1)
		t.end()
	}, 100)
})

// dom
test.todo('dom method')

//////// WITH ERROR FUNCTIONS /////////////

// fnALL method
test.cb('fnAll', t => {
	const m = Meander([1,2,Error('fake'),4])
	.fnAll(emit => input => {
		if(input instanceof Error)
			emit(3)

		else
			emit(input)
	})
	.combine(10)

	setTimeout(() => {
		t.deepEqual(m.value, [1,2,3,4])
		t.end()
	}, 200)
})

// log
test.cb('log', t => {
	Meander([' '])
	.log(x => t.end())
})

// catch method
test.cb('catch returns value of the error', t => {
	Meander([Error('fake')])
	.catch(x => {
		t.is(x.message, 'fake')
		t.end()
	})
})

test.cb('rejected promise does not trigger catch', t => {
	Meander(Promise.reject(23))
	.catch( t.fail )
	.map( x => {
		t.is(x, 23)
		t.end()
	})
})

// retry
test.cb('retry method that throws error', t => {
	Meander([2])
	.reduce((acc, n) => Error(+acc.message + n), Error('0'))
	.retry(2)
	.catch(x => {
		t.is(x.message, '6')
		t.end()
	})
})

test.cb('retry method that succeeds', t => {
	Meander([true])
	.reduce(acc => acc +1, 0)
	.map(x => x < 3 ? Error('fale') : x)
	.retry(2)
	.catch(t.fail)
	.map(x => {
		t.is(x, 3)
		t.end()
	})
})