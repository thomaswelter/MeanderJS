export default function Meander() {
	if(!(this instanceof Meander))
		return new Meander(...arguments) // no new keyword needed

	const args = Array.prototype.slice.call(arguments)
	const root = this

	/*
		every new method returns a new object with itself as the prototype.
		A reference to these objects is kept in contextReference.
		it's value might look like this (the names are actualy object references):
		
		let chain = Meander().map()
		chain.reduce()
		chain.combine().log()

		{
			root: [map],
			map: [reduce, combine],
			reduce: [],
			combine: [log],
			log: []
		}
	*/
	const contextReference = new Map([[this, []]])

	if(Meander.debugger && Meander.debugger.active)
		Meander.debugger.addInit(this, 'Meander', args)

	/*
		context: [input => emit( input ), 'name', ['input arguments', 500]]

		the methodReference of log looks like this:
		log: [input => (console.log(input), emit(input)), 'log', []]
	*/
	const methodReference = new Map

	// stack that holds next method call
	const nextFnStack = []

	this._convertMethod = (name, context, args, fn) => {
		const signature = Object.create(context)

		// prevent value from looking in prototype
		signature.value = undefined

		// significant performance tweak on long chains, prevents walking the prototype chain
		signature._convertMethod = this._convertMethod

		// reference to prev child
		signature._prev = this

		contextReference.get(context).push(signature)
		contextReference.set(signature, [])

		if(Meander.debugger && Meander.debugger.active)
			Meander.debugger.addInit(signature, name, args)

		// emit can fire multiple values
		const emit = (...args) => args.forEach(arg => next(signature)(arg))

		// fn is now: input => emit( )
		const setupFn = fn(...args)( emit, signature )

		// args is kept only for debugging
		methodReference.set(signature, setupFn)

		return signature
	}

	// push is not chainable and does not add to the contextReference, so it's defined seperately
	this.push = (...args) => args.forEach( arg => next(root)(arg) )

	// access root trough this property
	this._root = root

	// avoid recursion (stack overflow) by using a function stack
	const next = signature => input => {
		nextFnStack.push(next_wrapped(signature)(input))

		if(nextFnStack.length > 1)
			return

		while(nextFnStack.length) {
			nextFnStack[0]()
			nextFnStack.shift()
		}
	}

	// call the next method's function, input is the value of the previous method
	const next_wrapped = signature => input => () => {
		// assign last value
		signature.value = input

		if(Meander.debugger && Meander.debugger.active)
			Meander.debugger.addCall(signature, input)

		// if this is the last method and the value is an error it is uncaught
		if(!contextReference.get(signature).length && input instanceof Error)
			console.error('Uncaught Error: ' + input.message + '\n\tin Meander')

		const execNext = () => {
			// executes the actual function
			// does not need to return a values
			// the next value is obtained by calling the emit function (async)
			const activate = signature => input => {
				const fn = methodReference.get(signature)

				try { fn(input) }
				catch(e) {
					next(signature)(Error(e
						+ '\n\tin .'
						+ name
						+ '( '
						+ args.join('')
						// + formatValueToString(args, false, true, 50)
						+ ')'
					))
				}
			}

			// resolve Promises and Meanders before proceeding
			contextReference.get(signature).forEach(child => {
				if(input instanceof Promise)
					input.then( activate(child), activate(child) )

				else if(input instanceof Meander)
					input._debuggerIgnore( activate(child) )

				else
					activate(child)(input)
			})
		}

		if(Meander.debugger && Meander.debugger.pause)
			Meander.debugger(execNext)

		else
			execNext()
	}

	// then create event contextReference from arguments
	args.forEach(arg => {
		const listen = next(this)

		const isUrl = str => /^http:\/\//.test(str)

		if(typeof window !== 'undefined' && typeof module == 'undefined') {
			const bindEvent = el => {
				const events = {
					'INPUT': 'input',
					'TEXTAREA': 'input'
				}

				return events[el.nodeName] || 'click'
			}
				
			if( typeof arg == 'string' && !isUrl(arg)) {
				const el = document.querySelector(arg)
				el.addEventListener(bindEvent(el), listen)
			}

			if( arg === document )
				arg.addEventListener('DOMContentLoaded', listen)

			if( arg === window )
				arg.addEventListener('load', listen)

			if( arg instanceof Element)
				arg.addEventListener(bindEvent(arg), listen)
		}

		if( typeof arg == 'string' && isUrl(arg))
			fetch(new Request(arg)).then( listen )

		if( typeof arg == 'function' )
			arg( listen )

		if( typeof arg == 'number' )
			setTimeout(() => listen( arg ), 0) // wait one cycle to give script time to attach follow up function

		if( Array.isArray(arg) )
			setTimeout(() => arg.forEach( listen ), 0)

		if( arg instanceof Promise )
			arg.then( listen, listen )

		if( arg instanceof Meander)
			arg._debuggerIgnore( listen )
	})

	return this
}