window.Meander.debugger = (() => {
	let debugValues = []
	let timer

	const resetTimer = () => {
		clearTimeout(timer)
		timer = setTimeout(run, root.debug)
	}

	// used as formatting function for debugging
	const formatValueToString = (values, prettify, isArgument, maxLength) => {
		const unwrap = value => {
			if(value instanceof Meander)
				return 'Meander'

			if(value instanceof Promise)
				return 'Promise'

			if(['[object Object]', ''].indexOf(value + '') !== -1 || Array.isArray(value))
				return JSON.stringify(value, null, prettify && '\t')

			return prettify? value + '' : (value + '').replace(/\n|\t/g, '')
		}

		if(Array.isArray(values) && isArgument)
			values = values.map(v => unwrap(v)).join(', ')
			
		else if(Array.isArray(values))
			values = '[' + values.map(v => unwrap(v)).join(', ') + ']'

		else
			values = unwrap(values)

		if(values.length > maxLength)
			values = values.slice(0, maxLength) + '...'

		return values
	}

	const run = () => {
		if(!debugValues.length)
			return

		// extend with [fn, name, args]
		debugValues = debugValues.map(item => item.concat(methodReference.get(item[2])))

		// [timestamp, result, signature, fn, name, args, timeColor]
		// adds the same time stamp when withing 5ms
		debugValues = debugValues.map((item, i) => {
			const ms = 5

			if(!debugValues[i +1])
				return item

			if(!item[6])
				item[6] = item[0]

			if(debugValues[i +1][0] - item[0] < ms)
				debugValues[i +1].push(item[6])

			return item
		})

		// the colors used as time devider
		const colors = ['lightgrey', 'blue', 'orange', 'green', 'red', 'purple', 'yellow', 'brown']

		// create array with timestamps (as created above) without duplicates
		const times = debugValues.map(x => x[6]).filter((x, i, r) => r.indexOf(x) == i)

		// convert the timestamp in debugValues into an color
		let timesObject = {}
		times.forEach((t, i) => timesObject[t] = colors[i % colors.length])
		debugValues.forEach(item => item[6] = timesObject[item[6]])

		const step = signature => {		
			const values = debugValues.filter(item => item[2] == signature)

			// print values that are emitted to the console
			values.forEach((item, i) => {
				const [timestamp, result, signature, fn, name, args, timeColor] = item
				const date = new Date(timestamp)
				const dateString = date.getHours() + ':'
					+ date.getMinutes() + ':'
					+ date.getSeconds() + '.'
					+ (date.getMilliseconds() + (timestamp % 1))
				const padding = n => val => val.length > n? val : (val + Array(n).join(' ')).slice(0, n)
				const resultColor = result instanceof Error? 'red' : 'blue'

				// function used for passing Meander into meander
				if(name == '_debuggerIgnore')
					return

				console.log('%c %c %s%c %s %c<-- %c%s( %c%s ) %s',
					'background: ' + timeColor,
					'color: grey', padding(15)(dateString.slice(0, 15)),
					'color: ' + resultColor, padding(10)(formatValueToString(result, true)),
					'color: lightgrey',
					'color: grey', name,
					'color: grey', formatValueToString(args, false, true, 50),
					i % 2? ' ' : '' // prevent chrome from combining values
				)
			})

			// if this is not the end of the chain a function did not (yet) emit a value, log that so we know which function is called
			contextReference.get(signature).forEach(next => {
				if(values.length > debugValues.filter(item => item[2] == next).length) {
					const [fn, name, args] = methodReference.get(next)

					const comment = (v, i) => {
						console.log('  %c%s %s <-- %s( %s ) %s', 'color: lightgrey',
							Array(15 + 1).join('-') + ' ',
							Array(10).join(' '),
							name,
							formatValueToString(args, null, true, 50),
							i % 2? ' ' : ''
						)
					}

					[...Array(values.length - debugValues.filter(item => item[2] == next).length)].forEach(comment)
				}
			})
		}

		// stack based aproach instead of recursion to prevent stack overflow errors

		let stack = [null]
		let current = root

		console.group('Meander')

		while(stack.length) {
			step(current)
			let signatures = contextReference.get(current).slice()
			if(!signatures.length) {
				current = stack.pop()
				console.groupEnd()

				if(stack.length)
					console.group('')

				continue
			}

			if(signatures.length > 1)
				console.group('')

			current = signatures.shift()
			stack = stack.concat(signatures)
		}

		console.groupEnd()

		// each debug run starts out fresh
		debugValues = []
	}

	return {
		addCall: (signature, input) => {
			const time = () => Date.now() - (Date.now() - performance.timeOrigin) + performance.now()
			resetTimer()
			debugValues.push([time(), input, signature])
		},

		addInit: () => {},

		active: true
	}
})()