import './meander.js'

const calls = new Map
const inits = new Map

const keys = {
	8: 'backspace',
	13: 'enter',
	27: 'escape',
	32: 'space',
	48: '0',
	49: '1',
	50: '2',
	51: '3',
	52: '4',
	53: '5',
	54: '6',
	55: '7',
	56: '8',
	57: '9',
	37: 'left',
	38: 'up',
	39: 'right',
	40: 'down',
}

Meander.debugger = () => {
	Meander.debugger.active = true
	Meander.debugger.pause = true

	window.addEventListener('keydown', ev => {
		if(!ev.altKey || !ev.ctrlKey)
			return

		if(!keys[ev.keyCode])
			return

		const key = keys[ev.keyCode]
		console.log(key);
	})

	setTimeout(() => {
		let items = ['']
		let i = 1
		for(let [name, args, time] of inits.values()) {
			items[0] += '%c%s %c%s > %c.%s( %s )\n'
			items = items.concat(
				'color: black', i++,
				'color: lightgrey', time,
				'color: black', name,
				args.join(', ')
			)
		}

		console.log(...items)
	}, 100)
}

Meander.debugger.addCall = (signature, input) => {
	calls.set(signature, input, Date.now())
}

Meander.debugger.addInit = (signature, name, args) => {
	inits.set(signature, [name, args, Date.now()])
}