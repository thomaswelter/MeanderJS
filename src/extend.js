import Meander from './meander.js'

export const extendWithError = object => {
	// convert the Meander.prototype functions into chainable form
	for(let key in object) {
		Meander.prototype[key] = function() {
			return this._convertMethod(key, this, Array.prototype.slice.call(arguments), object[key])
		}
	}
}

export const extend = object => {
	// convert the Meander.prototype functions into chainable form
	const filterError = fn => (...args) => emit => {
		const setupFn = fn(...args)(emit)

		return input => {
			if(input instanceof Error)
				return emit( input )

			setupFn( input )
		}
	}

	for(let key in object) {
		Meander.prototype[key] = function() {
			return this._convertMethod(key, this, Array.prototype.slice.call(arguments), filterError(object[key]))
		}
	}
}