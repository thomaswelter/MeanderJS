export const fn = fn => emit =>
	fn(emit)

export const map = fn => emit =>
	input =>emit( fn(input) )

export const reduce = (fn, initial) => emit => {
	let acc = initial

	return input => emit( acc = fn(acc, input) )
}

export const filter = fn => emit =>
	input => fn(input) && emit(input)

export const delay = ms => emit =>
	input => setTimeout(() => emit( input ), ms)

export const combine = ms => emit => {
	let buffer = []

	return input => {
		if(!buffer.length)
			setTimeout(() => (emit(buffer), buffer = []), ms)

		buffer.push(input)
	}
}

export const throttle = ms => emit => {
	let buffer = []
	let running = false

	const next = () => setTimeout(() => {
		if(buffer.length) {
			emit( buffer.shift() )
			return next()
		}

		running = false
	}, ms)

	return input => {
		buffer.push( input )

		if(running)
			return

		running = true
		emit( buffer.shift() )
		next()
	}
}

export const distinct = () => emit => {
	let prev = undefined

	return input => {
		if(prev === input)
			return

		prev = input
		emit( input )
	}
}

export const first = (n = 1) => emit => {
	let count = 0

	return input => {
		if(count >= n)
			return

		count += 1
		emit( input )
	}
}

export const skip = n => emit => {
	let skipped = 0

	return input => {
		if(skipped >= n)
			return emit( input )

		skipped++
	}
}

export const timestamp = () => emit =>
	input => emit( Date.now() )

export const count = () => emit => {
	let counter = 0

	return input => {
		counter++

		emit( counter )
	}
}

export const average = () => emit => {
	let count = 0
	let average = 0

	return input => {
		if(typeof input !== 'number')
			return emit(Error('average: input was not a number'))

		count++
		average = ((count -1 ) * average + input) / count
		emit( average )
	}
}

export const sum = () => emit => {
	let total = 0

	return input => {
		if(typeof input !== 'number')
			return emit(Error('sum: input was not a number'))

		total += input
		emit( total )
	}
}

export const min = () => emit => {
	let min = Infinity

	return input => {
		if(typeof input !== 'number')
			return emit(Error('min: input was not a number'))

		min = Math.min(min, input)
		emit( min )
	}
}

export const max = () => emit => {
	let max = -Infinity

	return input => {
		if(typeof input !== 'number')
			return emit(Error('max: input was not a number'))

		max = Math.max(max, input)
		emit( max )
	}
}

export const test = fn => emit =>
	input => emit( !!fn(input) )

export const hold = token => emit => {
	let buffer = []

	return input => {
		if(input === token) {
			emit( ...buffer )
			buffer = []
			return
		}

		buffer.push( input )
	}
}

export const debounce = ms => emit => {
	let running = false

	return input => {
		if(running)
			return

		emit( input )
		running = true
		setTimeout(() => running = false, ms)
	}
}

/*
	dom value to HTML, example ussage:
	
	input = [
		{body: 'example', likes: 57, time: 234234},
		{body: 'example 2', likes: 42, time: 105352}
	]

	<ul id="comments">
		<p>comment body</p>
		<button>Remove</button>
		<span>0 Likes</span>
		<span>some time ago</span>
	</ul>

	const listen = (event, fn) => element => element.addEventListener(event, fn)

	.dom('#comments', (item, i, wrapper) => ({
		p 		: item.body
		button	: first && listen('click', data.remove(i)),
		span 	: [item.likes + ' Likes', new Date(item.time)]
	}))
*/
export const dom = (...args) => emit => {
/*
	if(typeof fn !== 'function')
		throw Error('fn should be a function\n\t.dom( prefix, fn )')*/

	const isFn = val => typeof val == 'function'
	const isValue = val => ['string', 'number'].indexOf(typeof val) > -1
	const asArray = val => Array.isArray(val)? val : [val]
	const takeFirst = (arr, fn) => arr[0] && fn(arr[0])
	const runIf = (fn1, question, fn2) => question? fn1.map(fn2) : fn1
	const setInnerHTML = element => value => element.innerHTML = value
	const forEachTag = (fn, element) => ([tagName, value]) => Array.prototype.slice.call(element.querySelectorAll(tagName)).map(fn(value))
	const runWithElement = first => value => element => {
		let round = runIf(
			asArray(value).map(value => isFn(value)? value(element) : value),
			first,
			value => isFn(value)? value() : value
		)

		takeFirst( round.filter(isValue), setInnerHTML(element) )
	};

	// Function only mode
	if(isFn(args[0])) {
		let first = true
		let fn = args[0]

		return input => {
			Object.entries(fn(input))
				.forEach( forEachTag(runWithElement(first), document) )

			first = false
			emit(input)
		}
	}

	// use prefix as template mode
	else if(isFn(args[1])) {
		let prefix = args[0]
		let fn = args[1]
		let domRef = []

		const element = prefix instanceof Element
			? prefix
			: document.querySelector(prefix)
		const template = element.innerHTML
		element.innerHTML = ''

		return input => {
			asArray(input).forEach((item, i) => {
				// item = {title: 'first title', active: true}

				// search for item in domReference
				const itemAtIndex = domRef.map(x => x[0]).indexOf(item)

				let first = false
				let [value, wrapper] = itemAtIndex == -1
					? [item, false]
					: domRef.splice(itemAtIndex, 1)[0]

				// create if non existent
				if(!wrapper) {
					first = true
					wrapper = document.createElement('span')
					wrapper.innerHTML = template
					element.appendChild(wrapper)
				}

				// place at the right index
				wrapper.parentNode.insertBefore(wrapper, wrapper.parentNode.children[i])

				/*
					selectorObject = {
						h1: 'first title',
						'.item': element => element.style.backgroundColor = 'green'
					}
				*/
				const selectorObject = fn(item, i, wrapper)

				if(typeof selectorObject !== 'object')
					throw Error('fn should return an Object\n\t.dom( prefix, fn )')

				Object.entries(selectorObject)
					.forEach( forEachTag(runWithElement(first), wrapper) )
			})

			// remove unused nodes
			domRef.forEach(item => item[1].remove())

			// reference for next time, include wrappers
			domRef = asArray(input).map((x, i) => [x, element.children[i]])

			emit( input )
		}
	}

	// simple mode, set only one tagName
	else {
		let tagName = args[0]
		return input => {
			isValue(input)
				&& forEachTag(value => element => {
					['INPUT', 'TEXTAREA'].indexOf(element.nodeName) == -1
						? element.innerHTML = value
						: element.value = value
				}, document)([tagName, input])
				&& emit(input)
		}
	}
}