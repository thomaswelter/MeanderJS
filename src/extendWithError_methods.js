export const fnAll = fn => emit =>
	fn(emit)

export const log = fn => emit =>
	input => (console.log(input), fn && fn(input), emit(input))

// variables cannot be named 'catch'
export const $catch = fn => emit => input => {
	if(input instanceof Error)
		return emit( fn(input) )

	emit( input )
}

export const retry = (maxTries = Infinity) => (emit, context) => {
	let tries = 0

	return input => {
		if(input instanceof Error && tries < maxTries) {
			setTimeout(() => context.push( context._root.value ), 0)
			tries++
			return
		}

		emit( input )
	}
}

export const _debuggerIgnore = fn => emit =>
	input => emit( fn( input ) )