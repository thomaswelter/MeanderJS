import Meander from './meander.js'
import {extend, extendWithError} from './extend.js'
import {
	fn,
	map,
	reduce,
	filter,
	delay,
	combine,
	throttle,
	distinct,
	first,
	skip,
	timestamp,
	count,
	average,
	sum,
	min,
	max,
	test,
	hold,
	debounce,
	dom
} from './extend_methods.js'
import {
	fnAll,
	log,
	$catch,
	retry,
	_debuggerIgnore
} from './extendWithError_methods.js'


Meander.extend = extend
Meander.extendWithError = extendWithError

Meander.extend({
	fn,
	map,
	reduce,
	filter,
	delay,
	combine,
	throttle,
	distinct,
	first,
	skip,
	timestamp,
	count,
	average,
	sum,
	min,
	max,
	test,
	hold,
	debounce,
	dom
})

Meander.extendWithError({
	fnAll,
	log,
	catch: $catch, // catch is a reserved keyword
	retry,
	_debuggerIgnore
})

if(typeof window === 'undefined')
	module.exports = Meander

export default Meander